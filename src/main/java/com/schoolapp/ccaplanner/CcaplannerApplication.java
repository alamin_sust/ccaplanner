package com.schoolapp.ccaplanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CcaplannerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CcaplannerApplication.class, args);
	}

}
